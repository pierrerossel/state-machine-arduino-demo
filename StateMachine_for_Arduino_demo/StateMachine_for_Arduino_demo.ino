// StateMachine demo
//
// By using the StateMachine class and your own state classes herited from the State base class,
// we can easyly code an application that has different tasks to do at different times and separate
// them in the state classes.
//
// Pierre Rossel   2020-12-03  Initial version
//                 2024-01-10  Move the state change to the state class

#include "States.h"

StateMachine sm;

void setup() {
  Serial.begin(115200);

  Serial.println("\n\nHello world of state machines");

  sm.changeState(new StateGlow());

}

void loop() {

  sm.loop();

}
