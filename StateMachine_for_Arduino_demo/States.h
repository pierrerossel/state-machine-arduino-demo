// This file declares and implements the State* classes, which are the child of State class
// and contains the logic of each state of our application.
//
// Pierre Rossel   2020-12-03  Initial version
//                 2024-01-10  Implement loop() functions separately to be able to change to any state at anytime

#include "StateMachine.h"

class StateBlink : public State {

    void enter() {
      Serial.println(">>>>>>>>>>>>> StateBlink.enter()");
      pinMode(LED_BUILTIN, OUTPUT);
    }

    // Loop is declared here, but implemented below, after the declaration of other states
    State * loop();

    void exit() {
      Serial.println("<<<<<<<<<<<<< StateBlink.exit()");
    }
};


class StateGlow : public State {

    long period = 5 * 1000L;

    void enter() {
      Serial.println(">>>>>>>>>>>>> StateGlow.enter()");
      pinMode(LED_BUILTIN, OUTPUT);
    }

    // Loop is declared here, but implemented below, after the declaration of other states
    State * loop();

    void exit() {
      Serial.println("<<<<<<<<<<<<< StateGlow.exit()");
    }
};


State * StateBlink::loop() {
  digitalWrite(LED_BUILTIN, millis() % 1000 > 500);

  return getStateTime() > 5000 ? new StateGlow() : NULL;
}


State * StateGlow::loop() {
  float intensity = cos(TWO_PI * getStateTime() / period) / 2 + 0.5; // 0.0 ... 1.0

  // Custom non blocking PWM code for non PWM pin
  long pwmPeriod = 20 * 1000L; // microseconds
  long timePwm = micros() % pwmPeriod;
  bool high = timePwm < pwmPeriod * intensity;
  digitalWrite(LED_BUILTIN, high);

  // Change state after some time.
  // Note: We can only create (new) a new object of a class
  // that has been already declared before this one.
  return getStateTime() > 5000 ? new StateBlink() : NULL;
}
